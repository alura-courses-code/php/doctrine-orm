# php-doctrine-orm

Base project to test doctrine ORM and learn how it works and how to configure it.

# Usage

1. Clone the Repository:
    ```
    git clone https://gitlab.com/alura-courses-code/php/php-doctrine-orm.git
    cd php-doctrine-orm
    ```

2. Install Dependencies:
    ```
    composer install
    ```

3. Run scripts in the `bin` folder:
    ```
    php bin/script_name.php
    ```

# Contribution

If you're interested in contributing to this project, feel free to open a merge request. We appreciate all forms of contribution!

# License

This project is licensed under the [MIT License](https://gitlab.com/alura-courses-code/php/php-doctrine-orm/-/blob/main/LICENSE). Refer to the LICENSE file for more details.
